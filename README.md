# Glossaico
Glossaico is a language learning application based on [LibreLingo](https://github.com/kantord/LibreLingo).

Its primary target is Linux Mobile devices but it also runs on any environment where Python, Qt and Kirigami are available.

![](screenshots/glossaico-pm.png)

At the moment, it contains three language courses:

- Spanish for English speakers
- German for English speakers
- Basque for English speakers

## Install

Glossaico is available as a flatpak package and it can be installed from flathub. First, [ensure](https://flatpak.org/setup/) that flatpak has been installed and the flathub repository has been added. Then, install Glossaico:

```
flatpak install org.codeberg.dimkard.glossaico
```

## Contribute

- Install the Kirigami UI components from the repository of your Linux distribution. E.g., on Ubuntu:

```sudo apt install qml-module-org-kde-kirigami2```

- Ensure that sqlite is installed (most probably) on your system

- Clone the repository and create a virtual environment:

```
python3 -m venv env/
```

- Activate it using the activate script:

```
source env/bin/activate
```

- Ensure that pip, wheel and setuptools exist:

```python3 -m pip install --upgrade pip setuptools wheel```

- Install the PySide2 build dependencies

  - qtbase5-dev
  - qtdeclarative5-dev
  - qtdeclarative5-private-dev
  - llvm
  - libclang-dev

with the package manager of your distribution

- Build and install PySide2:

```
wget https://download.qt.io/official_releases/QtForPython/pyside2/PySide2-5.15.6-src/pyside-setup-opensource-src-5.15.6.tar.xz
tar xvf pyside-setup-opensource-src-5.15.6.tar.xz
cd pyside-setup-opensource-src-5.15.6
mkdir build
cmake -B build -G Ninja -DCMAKE_INSTALL_PREFIX=../../env/ -DBUILD_SHARED_LIBS=True -DCMAKE_BUILD_TYPE=None -DPYTHON_EXECUTABLE=/usr/bin/python3 -DBUILD_TESTS=OFF ..
cmake --build build
cmake --install build
export LD_LIBRARY_PATH=/path/to/env/lib
```

- Install the editable developer version of the application:

```
pip install -e ".[dev]"
```

- Check the file(s) changed:

```
./check_file.sh NEW_OR_MODIFIED_FILE
```

- Run the tests:

```
python -m pytest -sv tests/
```

- Run the application

```
glossaico
```

- Create a [pull request](https://codeberg.org/dimkard/glossaico/pulls)

## Translate

You can translate Glossaico on the [Weblate](https://translate.codeberg.org/projects/glossaico/) instance provided by Codeberg.

## License
Glossaico is licensed under GPL v3+.
