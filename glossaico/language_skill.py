# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Set
from slugify import slugify
from glossaico.glossaico_types import SkillData


class LanguageSkill:
    """Class that provides the data that a skill consists of"""

    def __init__(self, challenge_data_calculator, progress_calculator) -> None:
        self._skill_data: SkillData = SkillData()
        self._challenge_data_calculator = challenge_data_calculator
        self._progress_calculator = progress_calculator
        self._levels: int = 0
        self.loaded = False

    def load(self, skill_data: SkillData):
        """Loads the skill data to the skill model"""
        if (
            (not skill_data.skill_id)
            or (not skill_data.name)
            or (not skill_data.course_name)
        ):
            raise ValueError("Missing skill information")

        self._skill_data = skill_data

        self._challenge_data_calculator.load(skill_data)
        self._levels = self._challenge_data_calculator.levels()

        self._progress_calculator.load(skill_data, self._levels)
        self.loaded = True

    def validateLoad(self):
        """Raise an error if the model has not been loaded"""
        if not self.loaded:
            raise RuntimeError("No skill has been loaded for calculation")

    @property
    def skillId(self) -> str:
        """Returns the id of the skill, defined in the course configuration file"""
        self.validateLoad()
        return self._skill_data.skill_id

    @property
    def displayName(self):
        """Returns the name of the skill"""
        self.validateLoad()
        return self._skill_data.name

    @property
    def words(self) -> list:
        """Returns the list of the words of the skill"""
        self.validateLoad()
        return self._skill_data.words

    @property
    def phrases(self) -> list:
        """Returns the list of the phrases of the skill"""
        self.validateLoad()
        return self._skill_data.phrases

    @property
    def name(self) -> str:
        """Returns the slugified name of the skill"""
        self.validateLoad()
        return slugify(self._skill_data.name)

    @property
    def courseName(self) -> str:
        """Returns the name of the course that the skill belongs in"""
        self.validateLoad()
        return self._skill_data.course_name

    @property
    def progress(self) -> float:
        """Calculates and returns the skill progress"""
        self.validateLoad()
        return self._progress_calculator.skillProgress()

    @property
    def levels(self) -> int:
        """Returns the number of levels of the skill"""
        self.validateLoad()
        return self._levels

    @property
    def challenges(self) -> list:
        """Returns the list of the challenges of the skill"""
        self.validateLoad()
        return self._challenge_data_calculator.validChallenges()

    @property
    def needsReview(self) -> bool:
        """Returns True if the skill has been completed but it needs review"""

        self.validateLoad()
        return self._progress_calculator.needsReview()

    @property
    def challengesPassed(self) -> Set[str]:
        """Returns the set of challenge ids marked as passed after answered correctly"""
        self.validateLoad()
        return self._progress_calculator.challengesPassed()
