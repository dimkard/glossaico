# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path


class CourseValidation:
    """Class that offers utilities to check course validity"""

    def __init__(self, image_dir: str, voice_dir: str):
        self._image_dir = image_dir
        self._voice_dir = voice_dir

    def voiceExists(self, audio_name: str):
        """Returns True if the audio provided is found in the file system"""
        audio_file = Path(self._voice_dir, f"{audio_name}.mp3")

        return audio_file.is_file()

    def imageExists(self, image_name: str, image_size: str = "normal"):
        """Returns True if the image provided is found in the file system.
        Image sizes: normal, tiny, tinier"""
        size_pattern = ".jpg" if image_size == "normal" else f"_{image_size}.jpg"
        image_file = Path(self._image_dir, image_name.replace(".jpg", size_pattern))

        return image_file.is_file()
