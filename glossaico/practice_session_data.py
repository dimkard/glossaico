# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime


class PracticeSessionData:
    """Contains the data of a completed practice session"""

    def __init__(
        self,
        # pylint: disable=too-many-arguments
        course_name: str,
        practice_session_dt: datetime,
        correct_challenges_amt: int,
        expected_challenges_amt: int,
        practiced_skill_id: str,
        passed_challenges: set,
    ):
        self.course = course_name
        self.practice_dt = practice_session_dt
        self.correct_challenges = correct_challenges_amt
        self.expected_challenges = expected_challenges_amt
        self.skill_id = practiced_skill_id
        self.passed = passed_challenges


class PassedChallenges:
    """Contains the challenges that have been marked as passed"""

    def __init__(self, challenge_id: str):
        self.challenge_id = challenge_id
