# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QObject, Signal, Property, Slot
from glossaico.property_store import PropertyStore
from glossaico.app_configuration import ApplicationConfiguration


class QConfiguration(QObject):
    """Class that manages the configuration of the application
    and exposes qt properties
    """

    updated = Signal()

    def __init__(self, store: PropertyStore):
        QObject.__init__(self)

        self._store: PropertyStore = store
        self._cfg: ApplicationConfiguration = store.get()

        # pylint: disable=no-member
        self.silentModeChanged.connect(self.updateStore)
        self.installedDataCheckSumChanged.connect(self.updateStore)
        self.showBetaCoursesChanged.connect(self.updateStore)

    @Slot()
    def updateStore(self):
        """Upserts the modified configuration to the property store"""
        self._store.upsert(self._cfg)

    def readSilentMode(self) -> bool:
        """Returns True if no sound should be reproduced by the application"""
        return self._cfg.silentMode

    def setSilentMode(self, val: bool):
        """Controls the configuration option that should be set to True
        if no sound should be reproduced by the application"""
        # pylint: disable=no-member
        self._cfg.silentMode = val
        self.silentModeChanged.emit(val)

    silentModeChanged = Signal(bool)

    def readShowBetaCourses(self) -> bool:
        """Returns True if beta courses should be shown"""
        return self._cfg.showBetaCourses

    def setShowBetaCourses(self, val: bool):
        """Controls the configuration option that should be set to True
        if beta courses should be shown"""
        # pylint: disable=no-member
        self._cfg.showBetaCourses = val
        self.showBetaCoursesChanged.emit()
        self.coursesUpdated.emit(val)

    @Signal
    def showBetaCoursesChanged(self):
        """Emitted when showBetaCourses changes"""

    coursesUpdated = Signal(bool)

    def readInstalledDataCheckSum(self):
        """Returns the sha256 checksum of the installed glossaico-data package"""
        return self._cfg.installedDataCheckSum

    def setInstalledDataCheckSum(self, val: str):
        """Sets the configuration option that returns the sha256 checksum of the
        installed glossaico-data package"""
        # pylint: disable=no-member
        self._cfg.installedDataCheckSum = val
        self.installedDataCheckSumChanged.emit()
        self.coursesUpdated.emit(self.showBetaCourses)

    @Signal
    def installedDataCheckSumChanged(self):
        """Emitted when the installed data checksum changes"""

    def readOptionsDisplayAmt(self) -> int:
        """Returns the amount of candidate answers on option challenges"""
        return self._cfg.OPTIONS_ANSWERS_AMT

    def readCardsDisplayAmt(self) -> int:
        """The amount of pictures displayed on card challenges"""
        return self._cfg.CARD_PICTURES_AMT

    silentMode = Property(bool, readSilentMode, setSilentMode, notify=silentModeChanged)
    installedDataCheckSum = Property(
        str,
        readInstalledDataCheckSum,
        setInstalledDataCheckSum,
        notify=installedDataCheckSumChanged,
    )
    showBetaCourses = Property(
        bool,
        readShowBetaCourses,
        setShowBetaCourses,
        notify=showBetaCoursesChanged,
    )
    optionsDisplayAmt = Property(int, readOptionsDisplayAmt, constant=True)
    cardsDisplayAmt = Property(int, readCardsDisplayAmt, constant=True)
