# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QObject, Signal, Slot, Property
from glossaico.course_update_manager import CourseUpdateManager


class QCourseUpdateManager(QObject):
    """Class that exposes the course data update to QML"""

    dataUpdated = Signal(str)

    def __init__(self, app_data_dir: str, cache_dir: str, q_configuration):
        QObject.__init__(self)

        self._cfg = q_configuration
        self._upd_manager = CourseUpdateManager(
            app_data_dir, cache_dir, self._cfg.installedDataCheckSum
        )

    @Slot()
    def updateCourseContent(self):
        """Exposes the course update method"""
        try:
            upd_checksum = self._upd_manager.updateCourseContent()
            self._cfg.installedDataCheckSum = upd_checksum
            self.dataUpdated.emit("")
        except RuntimeError as exc:
            self.dataUpdated.emit(str(exc))

    @Slot(result=int)
    def dataUpdateType(self) -> int:
        """Exposes the data update type"""
        return self._upd_manager.dataUpdateType()

    def readUpdateTypeCodes(self) -> dict:
        """Returns the dictionary that contains the update type codes"""
        return self._upd_manager.update_type_codes

    @Slot(result=bool)
    def canUpdate(self) -> bool:
        """Exposes the method that returns true if course data can be updated"""
        return self._upd_manager.canUpdate()

    updateTypeCodes = Property("QVariantMap", readUpdateTypeCodes, constant=True)
