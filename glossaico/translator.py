# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
from pathlib import Path
import logging
from glossaico import APP_COMPONENT_NAME

_ = gettext.gettext
ngettext = gettext.ngettext

src_dir = Path(__file__).resolve().parent

try:
    translation = gettext.translation(f"{APP_COMPONENT_NAME}")
    if translation:
        translation.install()
        _ = translation.gettext
        ngettext = translation.ngettext
    else:
        logging.warning("The application will not be translated")
except FileNotFoundError:
    logging.warning("No translations files found")
