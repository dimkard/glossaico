# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from random import shuffle, choice
from librelingo_utils import remove_control_characters_for_display as no_ctl_chars  # type: ignore
from glossaico.glossaico_types import (
    ShortInputChallengeQuestion,
    ChallengeAnswer,
    OptionsChallengeQuestion,
    CardsChallengeQuestion,
    ListeningChallengeQuestion,
    ChipsChallengeQuestion,
)


class ChallengeQACreator:
    """Class that constructs challenge question and answers"""

    option_challenges_amt = 3
    card_challenges_amt = 4

    def __init__(self, phrases: list, words: list, validator):
        self._function_map = {
            "shortInput": self._shortInputChallenge,
            "options": self._optionsChallenge,
            "cards": self._cardsChallenge,
            "listeningExercise": self._listeningChallenge,
            "chips": self._chipsChallenge,
        }
        self.phrases = phrases
        self.words = words
        self.validator = validator

    def qaTuple(self, challenge: dict) -> tuple:
        "Creates the question / answer tuple of the challenge given"
        data_func = self._function_map[challenge["type"]]
        return data_func(challenge)

    @staticmethod
    def asListMember(candidate):
        """If the input argument is not a list, it is added to a list and the
        list is returned. Otherwise, the input argument is returned without any
        change"""
        wrapper_list = []
        if not isinstance(candidate, list):
            wrapper_list.append(candidate)
        else:
            wrapper_list = candidate

        return wrapper_list

    def _chipsChallenge(self, challenge: dict) -> tuple:
        challenge_chips = challenge["chips"]
        shuffle(challenge_chips)
        question = ChipsChallengeQuestion(
            cid=challenge["id"],
            group=challenge["group"],
            priority=challenge["priority"],
            meaningInSourceLanguage=" ".join(
                str(x["word"]) for x in challenge["phrase"]
            ),
            chips=challenge_chips,
        )

        answer = ChallengeAnswer(
            cid=challenge["id"],
            formInTargetLanguage=self.__class__.asListMember(challenge["solutions"]),
        )

        return (question, answer)

    def _listeningChallenge(self, challenge: dict) -> tuple:
        question = ListeningChallengeQuestion(
            cid=challenge["id"],
            group=challenge["group"],
            priority=challenge["priority"],
            meaning=challenge["meaning"],
            audio=challenge["audio"],
        )

        answer = ChallengeAnswer(
            cid=challenge["id"],
            formInTargetLanguage=self.__class__.asListMember(challenge["answer"]),
        )

        return (question, answer)

    def _shortInputChallenge(self, challenge: dict) -> tuple:
        question = ShortInputChallengeQuestion(
            cid=challenge["id"],
            group=challenge["group"],
            priority=challenge["priority"],
            meaningInSourceLanguage=" ".join(
                str(x["word"]) for x in challenge["phrase"]
            ),
            pictures=challenge["pictures"],
        )

        answer = ChallengeAnswer(
            cid=challenge["id"],
            formInTargetLanguage=self.__class__.asListMember(
                challenge["formInTargetLanguage"]
            ),
        )

        return (question, answer)

    def _optionsChallenge(self, challenge: dict) -> tuple:
        challenge_options = [challenge["formInTargetLanguage"]]
        candidates = [
            no_ctl_chars(p.in_target_language[0])
            for p in self.phrases
            if (
                no_ctl_chars(p.in_target_language[0])
                != challenge["formInTargetLanguage"]
            )
        ]

        for i in range(self.option_challenges_amt - 1):
            challenge_options.append(candidates[i])

        shuffle(challenge_options)

        question = OptionsChallengeQuestion(
            cid=challenge["id"],
            group=challenge["group"],
            priority=challenge["priority"],
            meaningInSourceLanguage=challenge["meaningInSourceLanguage"],
            options=challenge_options,
        )

        answer = ChallengeAnswer(
            cid=challenge["id"],
            formInTargetLanguage=self.__class__.asListMember(
                challenge["formInTargetLanguage"]
            ),
        )

        return (question, answer)

    def _cardsChallenge(self, challenge: dict) -> tuple:
        card_options = [
            {
                "formInTargetLanguage": challenge["formInTargetLanguage"],
                "picture": choice(challenge["pictures"]).split(".")[0],
            }
        ]

        for c in self.words:
            # avoid adding again the correct answer
            if (
                challenge["formInTargetLanguage"] not in c.in_target_language
            ) and c.pictures:
                new_card_pic = choice(c.pictures)
                if self.validator.imageExists(f"{new_card_pic}.jpg", "tiny"):
                    card_options.append(
                        {
                            "formInTargetLanguage": c.in_target_language[0],
                            "picture": new_card_pic,
                        }
                    )

            if len(card_options) == self.card_challenges_amt:
                break

        shuffle(card_options)

        question = CardsChallengeQuestion(
            cid=challenge["id"],
            group=challenge["group"],
            priority=challenge["priority"],
            meaningInSourceLanguage=challenge["meaningInSourceLanguage"],
            options=card_options,
        )

        answer = ChallengeAnswer(
            cid=challenge["id"],
            formInTargetLanguage=self.__class__.asListMember(
                challenge["formInTargetLanguage"]
            ),
        )

        return (question, answer)
