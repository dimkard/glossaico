# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from abc import ABC, abstractmethod


class DatabaseStore(ABC):
    """The interface of the database store classes"""

    @abstractmethod
    def get(self, record_id: str):
        """Returns the model with this id from the database"""

    @abstractmethod
    def add(self, model):
        """Adds the model provided to the database"""
