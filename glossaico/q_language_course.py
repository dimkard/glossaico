# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Optional
from librelingo_types import Course
from PySide2.QtCore import QObject, Signal, Slot, Property
from glossaico.language_course import CourseLoader


class QCourseData(QObject):
    """Exposes the data of language course"""

    def __init__(self, course: Course, name: str) -> None:
        QObject.__init__(self)
        self._course = course
        self._name = name

    def readName(self) -> str:
        """Returns the name of the language course"""
        return self._name

    @Signal
    def nameChanged(self):
        """Emitted when the course name is set"""

    def readTargetLanguageName(self) -> str:
        """Returns the name of the target language of the course"""
        return self._course.target_language.name

    @Signal
    def targetLanguageNameChanged(self):
        """Emitted when the target language of the course is set"""

    def readTargetLanguageCode(self):
        """Returns the code of the target language of the course"""
        return self._course.target_language.code

    @Signal
    def targetLanguageCodeChanged(self):
        """Emitted when the code of the target language of the course is set"""

    def readSourceLanguage(self) -> str:
        """Returns the name of the source language of the course"""
        return self._course.source_language

    @Signal
    def sourceLanguageChanged(self):
        """Emitted when the source language of the course is set"""

    def readSpecialCharacters(self) -> list:
        """Returns the list of the special characters needed by the
        language course
        """
        return self._course.special_characters

    @Signal
    def specialCharactersChanged(self):
        """Emitted when the special characters of the course are set"""

    def readModules(self) -> list:
        """Returns the list of the modules included in the language course"""
        return self._course.modules

    @Signal
    def modulesChanged(self):
        """Emitted when the list of the modules of the course is set"""

    name = Property(str, readName, notify=nameChanged)
    targetLanguageName = Property(
        str, readTargetLanguageName, notify=targetLanguageNameChanged
    )
    targetLanguageCode = Property(
        str, readTargetLanguageCode, notify=targetLanguageCodeChanged
    )
    sourceLanguage = Property(list, readSourceLanguage, notify=sourceLanguageChanged)
    specialCharacters = Property(
        list, readSpecialCharacters, notify=specialCharactersChanged
    )
    courseModules = Property(list, readModules, notify=modulesChanged)


class QCourseLoader(QObject):
    """Class that exposes the course loader to QML"""

    loaded = Signal()

    def __init__(self, cache_dir, course_config_dir, installed_data_check_sum) -> None:
        QObject.__init__(self)
        self._course_loader = CourseLoader(
            cache_dir, course_config_dir, installed_data_check_sum
        )
        self._course_data: Optional[QCourseData] = None

    @Slot(str)
    def load(self, course_name: str):
        """Loads the data of the language course provided"""
        course: Course = self._course_loader.load(course_name)
        self._course_data = QCourseData(course, course_name)
        # pylint: disable=no-member
        self.loaded.emit()

    def readCourseData(self):
        """Returns the course data of the course loaded"""
        return self._course_data

    @Signal
    def courseDataChanged(self):
        """Emitted when the course data property is changed"""

    courseData = Property(QCourseData, readCourseData, notify=courseDataChanged)
