# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

APP_COMPONENT_NAME = "glossaico"
APP_NAME = "Glossaico"
APP_VERSION = "1.2.90"
APP_URL = "https://codeberg.org/dimkard/glossaico"
APP_BUG_URL = "https://codeberg.org/dimkard/glossaico/issues"
APP_AUTHOR = "Dimitris Kardarakos"
APP_AUTHOR_EMAIL = "dimkard@posteo.net"
APP_AUTHOR_WEB_ADDR = "https://codeberg.org/dimkard"
APP_MAINTAINER = "Dimitris Kardarakos"
APP_MAINTAINER_EMAIL = "dimkard@posteo.net"
APP_DESCRIPTION = "Language learning application"
APP_COPYRIGHT = "© 2022 Dimitris Kardarakos"
APP_CREDITS = [
    {
        "name": "LibreLingo contributors",
        "webAddress": "https://github.com/LibreLingo/LibreLingo#contributors",
    },
    {
        "name": "Breeze Icon Theme Developers",
        "webAddress": "https://invent.kde.org/frameworks/breeze-icons/-/blob/master/icons/AUTHORS",
    },
]
