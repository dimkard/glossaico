# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from abc import ABC, abstractmethod
from glossaico.database_store import DatabaseStore


class DatabaseStoreKeeper(ABC):
    """Class responsible to maintain a database store"""

    @abstractmethod
    def __enter__(self):
        return self

    @abstractmethod
    def __exit__(self, *args):
        self.rollback()

    @abstractmethod
    def commit(self):
        """Commit changes to database"""

    @abstractmethod
    def rollback(self):
        """Rollback database changes"""

    @abstractmethod
    def store(self) -> DatabaseStore:
        """Returns the store that this store keeper maintains"""
