# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QObject, Signal, Slot, Property


class QPracticeSessionFlow(QObject):
    """Exposes the practice session flow to the user interface"""

    def __init__(self, session_flow):
        QObject.__init__(self)

        self._session_flow = session_flow
        self._practicing = False

    @Slot()
    def finalizeSession(self):
        """It is called when a learning session -consisting of a set of
        challenges- ends. All challenges in the session have
        been answered or skipped.
        """
        self._session_flow.updatePracticeStorage()
        # pylint: disable=no-member
        self.sessionCompleted.emit()

    @Slot()
    def completeChallenge(self):
        """It is called when a challenge has finished, after having been
        answered or skipped by the user, and the users is moving on to
        the next challenge
        """
        if self.loadNextChallenge():
            # pylint: disable=no-member
            self.continueSession.emit()
        else:
            self.finalizeSession()

    @Signal
    def continueSession(self):
        """It is emitted when a new challenge has been loaded"""

    @Signal
    def sessionCanceled(self):
        """It is emitted when a learning session -consisting of a set of
        challenges- has been canceled
        """

    @Signal
    def sessionCompleted(self):
        """It is emitted when a learning session -consisting of a set of
        challenges- has been finalized
        """

    def readCurrentChallengeQuestion(self) -> dict:
        """Returns the question of the current challenge"""
        return self._session_flow.currentChallengeQuestion

    @Signal
    def currentChallengeQuestionChanged(self):
        """Emitted when the current challenge has changed"""

    @Slot(result=float)
    def practiceProgress(self) -> float:
        """Returns the percentage of the practice session that has been
        completed
        """
        return self._session_flow.practiceProgress

    def readChallengesAnswered(self) -> int:
        """Returns the amount of challenges already answered in the current
        practice session
        """
        return self._session_flow.challengesAnswered

    @Signal
    def challengesAnsweredChanged(self):
        """Emitted when the amount of challenges already answered in the
        current practice session has changed
        """

    @Slot(dict, bool, result=bool)
    def loadSessionChallenges(self, language_skill, silent_mode: bool) -> bool:
        """Loads the challenges to be practiced in the session.
        Returns the amount of challenges for practice"""

        self._practicing = self._session_flow.loadSessionChallenges(
            language_skill, silent_mode
        )
        # pylint: disable=no-member
        self.practicingChanged.emit()

        return self._practicing

    @Slot()
    def endPractice(self):
        """It is called when the user exits practicing"""
        self._practicing = False
        # pylint: disable=no-member
        self.practicingChanged.emit()

    @Slot(result=bool)
    def loadNextChallenge(self) -> bool:
        """If there are remaining challenges in the practice session, it
        initializes the challenge to be offered to the user, e.g.
        currentChallengeQuestion should be set accordingly. Returns True
        if such a challenge exists.
        """
        load_result = self._session_flow.loadNextChallenge()
        # pylint: disable=no-member
        self.currentChallengeQuestionChanged.emit()

        return load_result

    @Slot("QVariantMap", result=bool)
    def checkResult(self, answer_given: dict) -> bool:
        """Returns True if the answer given is the correct one for the current
        challenge
        """
        is_correct = self._session_flow.checkResult(answer_given)
        # pylint: disable=no-member
        self.challengesAnsweredChanged.emit()

        return is_correct

    def readPracticing(self) -> bool:
        """Returns True is the user is in a practice session"""
        return self._practicing

    @Signal
    def practicingChanged(self):
        """Emitted when the user enters or exits a practice session"""

    currentChallengeQuestion = Property(
        "QVariantMap",
        readCurrentChallengeQuestion,
        notify=currentChallengeQuestionChanged,
    )
    challengesAnswered = Property(
        int, readChallengesAnswered, notify=challengesAnsweredChanged
    )
    practicing = Property(bool, readPracticing, notify=practicingChanged)
