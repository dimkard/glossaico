# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from sqlalchemy import text
from sqlalchemy.future import Engine
from sqlalchemy.orm import Session


class DatabaseMigration:
    """Handle database schema changes"""

    def __init__(self, db_engine: Engine):
        self._db_engine = db_engine
        self._db_session = None

    def __enter__(self):
        self._db_session = Session(self._db_engine)

    def __exit__(self, *args):
        self._db_session.rollback()
        self._db_session.close()

    def migrate(self):
        """Manage database migration actions"""

        user_version = self._db_session.execute(
            text(
                """
            PRAGMA user_version
            """
            )
        )

        if user_version.scalar() == 0:
            self.migrateTo1()

    def migrateTo1(self):
        """Manage database migration actions from version 0 to 1"""

        logging.info("Migrating database from version 0 to 1")

        self._db_session.execute(
            text(
                """
            CREATE TABLE tmp_passed_challenges (
                id integer NOT NULL,
                challenge_id VARCHAR NOT NULL,
                practice_id INTEGER NOT NULL,
                PRIMARY KEY (id),
                FOREIGN KEY(practice_id) REFERENCES practice_session (id)
                )
            """
            )
        )

        self._db_session.execute(
            text(
                """
            INSERT INTO tmp_passed_challenges(challenge_id, practice_id)
            SELECT challenge_id, practice_id
            FROM passed_challenges
            """
            )
        )

        self._db_session.execute(
            text(
                """
            DROP TABLE passed_challenges
            """
            )
        )

        self._db_session.execute(
            text(
                """
            ALTER TABLE tmp_passed_challenges RENAME to passed_challenges
            """
            )
        )

        self._db_session.execute(
            text(
                """
            PRAGMA user_version = 1
            """
            )
        )

        logging.info("Database migration completed")
        self._db_session.commit()
