# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


class ApplicationConfiguration:
    """The application configuration model"""

    OPTIONS_ANSWERS_AMT = 3
    CARD_PICTURES_AMT = 4

    def __init__(
        self,
        installed_checksum: str = "",
        silent_mode: bool = False,
        config_version: str = "v3",
        show_beta_courses: bool = False,
    ):
        self._installed_check_sum: str = installed_checksum
        self._silent_mode: bool = silent_mode
        self._config_version: str = config_version
        self._show_beta_courses: bool = show_beta_courses

    @property
    def configVersion(self) -> str:
        """Returns the version of the configuration schema. It is useful when
        applications are updated and configuration data should be migrated.
        """
        return self._config_version

    @configVersion.setter
    def configVersion(self, val: str):
        self._config_version = val

    @property
    def installedDataCheckSum(self):
        """Returns the sha256 checksum of the installed glossaico-data package"""
        return self._installed_check_sum

    @installedDataCheckSum.setter
    def installedDataCheckSum(self, val: str):
        self._installed_check_sum = val

    @property
    def silentMode(self):
        """Returns True if no sound should be reproduced by the application"""
        return self._silent_mode

    @silentMode.setter
    def silentMode(self, val: bool):
        self._silent_mode = val

    @property
    def showBetaCourses(self):
        """Returns True if beta courses should be shown"""
        return self._show_beta_courses

    @showBetaCourses.setter
    def showBetaCourses(self, val: bool):
        self._show_beta_courses = val
