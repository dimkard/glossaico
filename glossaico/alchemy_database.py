# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from sqlalchemy import Table, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import registry, relationship
from sqlalchemy.future import Engine
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges


class AlchemyDatabase:
    """Class that loads the application database"""

    def __init__(self, db_url: str):
        self._url = db_url
        self._registry = registry()

    def _practiceSessionTable(self) -> Table:
        return Table(
            "practice_session",
            self._registry.metadata,
            Column("id", Integer, primary_key=True, autoincrement=True),
            Column("course", String, nullable=False),
            Column("practice_dt", DateTime, nullable=False),
            Column("correct_challenges", Integer),
            Column("expected_challenges", Integer),
            Column("skill_id", String, nullable=False),
        )

    def _passedChallengesTable(self) -> Table:
        return Table(
            "passed_challenges",
            self._registry.metadata,
            Column("id", Integer, primary_key=True, nullable=False),
            Column("challenge_id", String, nullable=False),
            Column("practice_id", ForeignKey("practice_session.id"), nullable=False),
        )

    def load(self) -> Engine:
        """Excutes the required initialization steps and returns a ready for
        sessions database engine"""
        engine = create_engine(self._url, echo=False, future=True)

        self._registry.map_imperatively(
            PracticeSessionData,
            self._practiceSessionTable(),
            properties={"passed": relationship(PassedChallenges, collection_class=set)},
        )

        self._registry.map_imperatively(PassedChallenges, self._passedChallengesTable())

        self._registry.metadata.create_all(engine)

        return engine
