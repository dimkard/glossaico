# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path


class DirectorySettings:
    """Class that provides the file system directories used by the application"""

    def __init__(self, gen_data_loc: str, gen_cache_loc: str, app_cmp_name: str):
        self._gen_data_loc: str = gen_data_loc
        self._gen_cache_loc: str = gen_cache_loc
        self._app_cmp_name: str = app_cmp_name
        self._initalizeDirectories()

    def _initalizeDirectories(self):
        for dir_path_name in (
            self.appDataDir,
            self.voiceDir,
            self.imageDir,
            self.courseDir,
            self.cacheDir,
            self.courseConfigDir,
            self.courseExportDir,
        ):
            Path(dir_path_name).mkdir(parents=True, exist_ok=True)

    @property
    def appDataDir(self) -> str:
        """Returns the directory where application data are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}"

    @property
    def soundDir(self) -> str:
        """Returns the directory where application sound files are stored into"""
        return f"{Path(Path(__file__).parent).resolve()}/sound"

    @property
    def iconDir(self) -> str:
        """Returns the directory where application icon files are stored into"""
        return f"{Path(Path(__file__).parent).resolve()}/icons"

    @property
    def imageDir(self) -> str:
        """Returns the directory where course image files are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}/images"

    @property
    def voiceDir(self) -> str:
        """Returns the directory where course voice files are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}/voice"

    @property
    def courseDir(self) -> str:
        """Returns the directory where course languages specific data are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}/courses"

    @property
    def codeLicensesDir(self) -> str:
        """Returns the directory where code licenses' text exists"""
        return f"{Path(Path(__file__).parent).resolve()}/data/code_licenses"

    @property
    def dataLicensesDir(self) -> str:
        """Returns the directory where data licenses' text exists"""
        return f"{Path(Path(__file__).parent).resolve()}/data/data_licenses"

    @property
    def cacheDir(self) -> str:
        """Returns the directory where application cache files are stored into"""
        return f"{self._gen_cache_loc}/{self._app_cmp_name}"

    @property
    def courseConfigDir(self) -> str:
        """Returns the directory where course languages configuration data are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}/courses/yaml"

    @property
    def courseExportDir(self) -> str:
        """Returns the directory where course languages exported data are stored into"""
        return f"{self._gen_data_loc}/{self._app_cmp_name}/courses/export"
