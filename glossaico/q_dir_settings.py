# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QObject, Property


class QDirSettings(QObject):
    """Exposes directory settings to Qt"""

    def __init__(self, dir_settings):
        QObject.__init__(self)
        self._dir_settings = dir_settings

    def readSoundDir(self) -> str:
        """Returns the directory where application sound files are stored into"""
        return self._dir_settings.soundDir

    def readIconDir(self) -> str:
        """Returns the directory where application icon files are stored into"""
        return self._dir_settings.iconDir

    def readImageDir(self) -> str:
        """Returns the directory where course image files are stored into"""
        return self._dir_settings.imageDir

    def readVoiceDir(self) -> str:
        """Returns the directory where course voice files are stored into"""
        return self._dir_settings.voiceDir

    soundDir = Property(str, readSoundDir, constant=True)
    iconDir = Property(str, readIconDir, constant=True)
    imageDir = Property(str, readImageDir, constant=True)
    voiceDir = Property(str, readVoiceDir, constant=True)
