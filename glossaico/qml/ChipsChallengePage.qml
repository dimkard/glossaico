/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

AbstractChallengePage {
    id: root

    property var questionChips: challengeData.chips
    property var answerChips: []

    function pushAnswer(pos, val, keyEvent=null) {
        var answer = root.answerChips
        answer.push(val)
        root.answerChips = answer

        var question = root.questionChips
        question.splice(pos, 1)
        root.questionChips = question

        if(keyEvent) {
            answerRepeater.itemAt(answerRepeater.count-1).focus = true
            answerRepeater.itemAt(answerRepeater.count-1).focusReason = Qt.TabFocusReason
        }
    }

    function pullAnswer(pos, val, keyEvent=null) {
        var question = root.questionChips
        question.push(val)
        root.questionChips = question

        var answer = root.answerChips
        answer.splice(pos, 1)
        root.answerChips = answer

        if(keyEvent) {
            questionRepeater.itemAt(questionRepeater.count-1).focus = true
            questionRepeater.itemAt(questionRepeater.count-1).focusReason = Qt.TabFocusReason
        }
    }

    challengeItem: columnLayout
    title: QGlTr._("Translate")

    onAnswerChipsChanged: root.answerGiven = { "answer": answerChips }

    ColumnLayout {
        id: columnLayout

        spacing: Kirigami.Units.largeSpacing
        width: parent.width

        Kirigami.Heading {
            text: QGlTr._("Translate<b> %1").arg(root.challengeData.meaningInSourceLanguage)
            wrapMode: Text.WordWrap
            bottomPadding: Kirigami.Units.largeSpacing * 2
            leftPadding: Kirigami.Units.largeSpacing

            Layout.fillWidth: true
        }

        Controls.Label {
            id: answerField

            Layout.fillWidth: true

        }

        Flow {
            spacing: Kirigami.Units.largeSpacing
            Repeater {
                id: answerRepeater

                model: root.answerChips

                delegate: Controls.Button {
                    text: modelData

                    KeyNavigation.left: answerRepeater.itemAt(model.index - 1)
                    KeyNavigation.right: answerRepeater.itemAt(model.index + 1)

                    Keys.onPressed: (event)=> {
                        if ((event.key == Qt.Key_Space) || (event.key == Qt.Key_Return)) {
                            pullAnswer(model.index, modelData, event.key)
                        }
                    }
                    onClicked: pullAnswer(model.index, modelData)
                }
            }

            Layout.fillWidth: true
        }

        Kirigami.Separator {
            Layout.fillWidth: true
        }

        Flow {
            spacing: Kirigami.Units.largeSpacing

            Repeater {
                id: questionRepeater

                model: root.questionChips

                delegate: Controls.Button {
                    text: modelData

                    KeyNavigation.left: questionRepeater.itemAt(model.index - 1)
                    KeyNavigation.right: questionRepeater.itemAt(model.index + 1)

                    Keys.onPressed: (event)=> {
                        if ((event.key == Qt.Key_Space) || (event.key == Qt.Key_Return)) {
                            pushAnswer(model.index, modelData, event.key)
                        }
                    }
                    onClicked: pushAnswer(model.index, modelData)
                }
            }

            Layout.fillWidth: true
        }
    }
}
