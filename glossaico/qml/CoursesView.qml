/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import QtQml.Models 2.15
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.ScrollablePage {
    id: root

    property bool showGlobalHandle: true
    title: QGlTr._("Courses")

    signal courseLoaded(Glossaico.QCourseData courseData)

    Kirigami.CardsListView {
        id: view

        currentIndex: 0
        model: QCoursesModel

        header: Kirigami.Heading {
            text: QGlTr._("Select course")
            width: parent.width
            wrapMode: Text.WordWrap
            topPadding: Kirigami.Units.smallSpacing
            leftPadding: Kirigami.Units.smallSpacing
        }

        delegate: Kirigami.AbstractCard {
            id: courseCard

            showClickFeedback: true

            header: Kirigami.Heading {
                text: model && model.target
                wrapMode: Text.WordWrap
            }

            contentItem: Controls.Label {
                wrapMode: Text.WordWrap
                text: model && model.description
            }

            highlighted: ListView.isCurrentItem

            function trigger() {
                QCourseLoader.load(model.name)
            }

            Keys.onReturnPressed: trigger()
            onClicked: trigger()
        }
    }

    Connections {
        target: QCourseLoader

        function onLoaded() {
            root.courseLoaded(QCourseLoader.courseData)
        }
    }
}
