/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtMultimedia 5.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.19 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.Page {
    id: root

    property int returnAnswerCode: 0
    property var challengeItem
    property var challengeData
    property var answerGiven: { "answer": null }
    property var specialCharacters: []
    property real progress
    property bool preventNavigation: true

    signal cancelSession

    Audio {
        id: answerSound

        property string audioName: ""

        autoLoad: false
        source: QDirSettings.soundDir + "/" + audioName + ".mp3"
    }

    Kirigami.PromptDialog {
        id: confirmCancelDialog

        title: QGlTr._("Cancel practice session?")

        subtitle: QGlTr._("Are you sure that you want to abort this practice session? Your progress so far in this session will not be saved.")

        standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel

        onAccepted: QPracticeSessionFlow.sessionCanceled()
        onRejected: close()

    }
    header: Item {
        id: headerItem

        implicitHeight: Kirigami.Units.gridUnit * 2

        Controls.ProgressBar {
            anchors.centerIn: parent

            from: 0
            to: 1
            value: root.progress
            width: root.width - (Kirigami.Units.gridUnit * 2)
        }
    }

    footer: MessageFooter {
        id: messageFooter

        text: ""

        leftActions: [
           Kirigami.Action {
                id: skipAction

                text: QGlTr._("Skip")
                visible: (root.state === "question") && messageFooter.visible

                onTriggered: QPracticeSessionFlow.completeChallenge()
            },
            Kirigami.Action {
                id: cancelAction

                text: QGlTr._("Cancel")
                visible: (root.state === "question") && messageFooter.visible
                shortcut: "Esc"

                onTriggered: {
                    confirmCancelDialog.open()
                    confirmCancelDialog.focus = true
                }
            }
        ]

        rightActions: [
            Kirigami.Action {
                id: continueAction

                text: QGlTr._("Continue")
                visible: (root.state !== "question") && messageFooter.visible

                onTriggered: QPracticeSessionFlow.completeChallenge()
            },
            Kirigami.Action {
              id: submitAction

              text: QGlTr._("Submit")
              visible: (root.state === "question") && messageFooter.visible
              enabled: root.answerGiven && root.answerGiven["answer"] && root.answerGiven["answer"].length > 0

              onTriggered:root.state = QPracticeSessionFlow.checkResult(answerGiven) ? "answered-correct" : "answered-wrong"
            }
        ]
    }

    states: [
        State {
            name: "question"
            PropertyChanges {target: root; returnAnswerCode: 0}
            PropertyChanges {target: challengeItem; enabled: true}
            PropertyChanges {target: messageFooter; text: ""; backgroundColor: Kirigami.Theme.backgroundColor}
        },
        State {
            name: "answered-correct"
            PropertyChanges {target: root; returnAnswerCode: 1}
            PropertyChanges {target: challengeItem; enabled: false}
            PropertyChanges {target: answerSound; audioName: "correct"}
            StateChangeScript {
                name: "playAudio"
                script: if(!QConfiguration.silentMode) answerSound.play()
            }
            PropertyChanges {target: messageFooter; text: QGlTr._("Correct!"); backgroundColor: Kirigami.Theme.positiveTextColor}
        },
        State {
            name: "answered-wrong"
            PropertyChanges {target: root; returnAnswerCode: -1}
            PropertyChanges {target: challengeItem; enabled: false}
            PropertyChanges {target: answerSound; audioName: "wrong"}
            StateChangeScript {
                name: "playAudio"
                script: if(!QConfiguration.silentMode) answerSound.play()
            }
            PropertyChanges {target: messageFooter; text: QGlTr._("Wrong"); backgroundColor: Kirigami.Theme.negativeTextColor}
        }
    ]

    transitions: Transition {
        to: "question"

        NumberAnimation {target: root; from:0; to: 1; property: "opacity"; easing.type: Easing.InOutQuad; duration: 1000}
    }

    Component.onCompleted: state = "question"
}
