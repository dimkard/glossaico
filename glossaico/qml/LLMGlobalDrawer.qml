/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Layouts 1.12
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.GlobalDrawer {
    id: root

    property var pageStack
    property var courses

    title: "Glossaico"
    titleIcon: QDirSettings.iconDir + "/mascot-jetpack-noshadow.svg"
    isMenu: !Kirigami.Settings.isMobile && handleVisible

    header: Kirigami.AbstractApplicationHeader {
        topPadding: Kirigami.Units.smallSpacing
        bottomPadding: Kirigami.Units.largeSpacing
        leftPadding: Kirigami.Units.largeSpacing
        rightPadding: Kirigami.Units.smallSpacing
        implicitHeight: Kirigami.Units.gridUnit * 2

        Kirigami.Heading {
            level: 2
            text: "Glossaico"
            Layout.fillWidth: true
        }
    }

    actions: [
        Kirigami.Action {
            text: QGlTr._("Select course")
            iconName: "select"

            onTriggered: {
                pageStack.clear()
                pageStack.push(courses)
            }
        },

        Kirigami.Action {
            text: QGlTr._("Configure")
            iconName: "settings-configure"

            onTriggered: {
                pageStack.clear()
                pageStack.push(settingsPage)
            }
        },

        Kirigami.Action {
            text: QGlTr._("About")
            iconName: "help-about"

            onTriggered: {
                pageStack.clear()
                pageStack.push(aboutPage)
            }
        }
    ]

    Component {
        id: settingsPage

        SettingsPage {}
    }

    Component {
        id: aboutPage

        Kirigami.AboutPage {
            property bool showGlobalHandle: true

            aboutData: QAppInfo.aboutData
        }
    }
}
