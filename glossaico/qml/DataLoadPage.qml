/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami

Kirigami.Page {
    id: root

    property var dataUpdateType: QCourseUpdateManager.dataUpdateType()

    title: {
        if (root.state === "error")
            return QGlTr._("An error has occured")
        else if(root.dataUpdateType === QCourseUpdateManager.updateTypeCodes['insert'])
            return QGlTr._("Welcome to Glossaico")
        else
            return QGlTr._("New course content available")
    }

    signal dataLoadCompleted()

    Connections {
        target: QCourseUpdateManager

        function onDataUpdated(err) {
            if (err) {
                root.state = 'error'
                errorMessage.text = err
            }
            else {
                root.dataLoadCompleted()
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        width: parent.width
        spacing: Kirigami.Units.largeSpacing * 2

        RowLayout {
            id: info

            spacing: Kirigami.Units.largeSpacing

            Image {
                source: QDirSettings.iconDir + "/mascot-jetpack-noshadow"

                Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                Layout.preferredWidth: height
            }

            Kirigami.Heading {
                id: loaderHeader

                text: {
                    if (root.state === "error")
                        return QGlTr._("The content of the courses cannot be updated.")
                    else if (root.dataUpdateType === QCourseUpdateManager.updateTypeCodes['insert'])
                        return QGlTr._("Welcome to Glossaico, the language learning application powered by LibreLingo.<p>Glossaico is free and open source software, and it makes use of images and courses released under various licenses.<p>Press <i>Licenses</i> to check the license terms. If you agree, please press <i>Continue</i>.")
                    else
                        return QGlTr._("New course content is available.<p>Please press <i>Continue</i> to update the language courses. Otherwise, press <i>Skip</i>.")
                }
                wrapMode: Text.WordWrap
                level: 3
                textFormat: Text.RichText

                Layout.fillWidth: true

            }


            Layout.fillWidth: true
        }

        Item {
            Layout.fillHeight: true
        }

        RowLayout {
            spacing: Kirigami.Units.largeSpacing

            Controls.Label {
                id: downloadingFile
                visible: false

                text: QGlTr._("Fetching course data")
            }

            Controls.ProgressBar {
                id: progressBar

                visible: false
                from: 0
                to: 100
                value: timer.value

                Layout.fillWidth: true
            }

            Layout.fillWidth: true
        }

        Kirigami.InlineMessage {
            id: errorMessage

            type: Kirigami.MessageType.Error

            Layout.fillWidth: true
        }
    }

    Timer {
        id: timer

        property int value: 0

        interval: 80
        repeat: true
        running: true

        onTriggered: {
            value = (value + 1) % 100
        }
    }

    footer: MessageFooter {
        id: downloadMessage

        rightActions: [
            Kirigami.Action {
                text: QGlTr._("Licenses")
                visible: root.dataUpdateType && (root.dataUpdateType === QCourseUpdateManager.updateTypeCodes['insert'])

                onTriggered: {
                    dataLicences.open()
                }
            },
            Kirigami.Action {
                text: QGlTr._("Skip")
                visible: root.dataUpdateType && (root.dataUpdateType === QCourseUpdateManager.updateTypeCodes['update'])

                onTriggered: {
                    root.dataLoadCompleted()
                }
            },
            Kirigami.Action {
                text: QGlTr._("Continue")

                onTriggered: {
                    root.state = "downloading"
                    QCourseUpdateManager.updateCourseContent()
                }
            }
        ]
    }

    DataLicences {
        id: dataLicences
    }

    states: [
        State {
            name: ""
            PropertyChanges {target: info; enabled: true}
            PropertyChanges {target: downloadingFile; visible: false}
            PropertyChanges {target: progressBar; visible: false}
            PropertyChanges {target: downloadMessage; visible: true}
            PropertyChanges {target: errorMessage; visible: false}
            PropertyChanges {target: errorMessage; text: ''}
         },

        State {
            name: "error"
            PropertyChanges {target: info; enabled: true}
            PropertyChanges {target: downloadingFile; visible: false}
            PropertyChanges {target: progressBar; visible: false}
            PropertyChanges {target: downloadMessage; visible: false}
            PropertyChanges {target: footer; visible: false}
            PropertyChanges {target: errorMessage; visible: true}
        },

        State {
            name: "downloading"
            PropertyChanges {target: info; enabled: false}
            PropertyChanges {target: downloadingFile; visible: true}
            PropertyChanges {target: progressBar; visible: true}
            PropertyChanges {target: downloadMessage; visible: false}
            PropertyChanges {target: errorMessage; visible: false}
            PropertyChanges {target: errorMessage; text: ''}
        }
    ]
}
