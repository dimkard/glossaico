/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami

Kirigami.ScrollablePage {
    id: root

    title: QGlTr._("Skills")

    signal skillSelected(var languageSkill, var skillData)

    Kirigami.PlaceholderMessage {
        anchors.centerIn: parent
        visible: view.count === 0
        width: parent.width - (Kirigami.Units.largeSpacing * 4)
        text: QGlTr._("No skill found")
    }

    Kirigami.CardsListView {
        id: view

        enabled: model && count > 0
        model: QSkillsModel
        currentIndex: -1

        delegate: Kirigami.AbstractCard {
            enabled: (model.skill_data.progress < 1) || model.skill_data.completed_stale
            showClickFeedback: true
            highlighted: ListView.isCurrentItem

            function trigger() {
                root.skillSelected(model.language_skill, model.skill_data)
            }

            contentItem: Item {
                implicitWidth: delegateLayout.implicitWidth
                implicitHeight: Kirigami.Units.gridUnit * 4

                RowLayout {
                    id: delegateLayout

                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                    }
                    spacing: Kirigami.Units.largeSpacing

                    Image {
                        source: model.skill_data.sample_image ? (QDirSettings.imageDir + "/" + model.skill_data.sample_image + "_tiny") : QDirSettings.iconDir + "/mascot-jetpack-noshadow.svg"

                        Layout.fillHeight: true
                        Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                        Layout.preferredWidth: height
                    }

                    ColumnLayout {

                        RowLayout {
                            Kirigami.Icon {
                                visible: model.skill_data.progress >= 1
                                implicitHeight: Kirigami.Units.iconSizes.smallMedium
                                source: "dialog-ok"
                                color: Kirigami.Theme.positiveTextColor
                            }

                            Kirigami.Heading {
                                level: 2
                                text: model.skill_data.name
                            }
                        }

                        Controls.Label {
                            elide: Text.ElideRight
                            text: model.skill_data.sample_words || model.skill_data.sample_phrases

                            Layout.fillWidth: true
                        }

                        Controls.Label {
                            elide: Text.ElideRight
                            text: QGlTr._("Needs review")
                            color: Kirigami.Theme.highlightColor
                            visible: model.skill_data.completed_stale

                            Layout.fillWidth: true
                        }

                        Controls.ProgressBar {
                            id: bar

                            from: 0
                            to: 1
                            visible: model.skill_data.progress < 1
                            value: model.skill_data.progress

                            Layout.fillWidth: true
                        }
                    }

                }
            }

            Keys.onReturnPressed: trigger()
            onClicked: trigger()
        }
    }
}
