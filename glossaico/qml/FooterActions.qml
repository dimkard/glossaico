/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.7
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.0
import org.kde.kirigami 2.19 as Kirigami

RowLayout {
    id: root

    property var layoutAlignment
    property var actions

    spacing: Kirigami.Units.largeSpacing

    Layout.fillWidth: true
    Layout.alignment: layoutAlignment

    Repeater {
        id: actionsRepeater

        model: root.actions

        delegate: Controls.Button {
            visible: modelData.visible
            action: modelData
            KeyNavigation.right: actionsRepeater.itemAt(model.index+1 % actionsRepeater.count)
            KeyNavigation.left: actionsRepeater.itemAt(model.index-1 % actionsRepeater.count)
            Keys.onReturnPressed: trigger()
        }
    }
}
