/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

AbstractChallengePage {
    id: root

    challengeItem: columnLayout
    title: QGlTr._("Which of these is ...")
    answerGiven: answerGroup.checkState === Qt.Unchecked ? "" : { "answer": answerGroup.checkedButton.text }

    Controls.ButtonGroup {
        id: answerGroup
    }


    Keys.onUpPressed: {
        if (answerGroup.checkState === Qt.Unchecked) {
            toggle()
        }
        else if (answerGroup.checkedButton.modelIndex !== 0) {
            repeater.itemAt(answerGroup.checkedButton.modelIndex - 1).toggle()
        }
    }

    Keys.onDownPressed: {
        if (answerGroup.checkState === Qt.Unchecked) {
            repeater.itemAt(0).toggle()
        }
        else if (answerGroup.checkedButton.modelIndex !== (repeater.count - 1)) {
            repeater.itemAt(answerGroup.checkedButton.modelIndex + 1).toggle()
        }
    }

    ColumnLayout {
        id: columnLayout

         anchors {
            top: parent.top
            right: parent.right
            left: parent.left
        }
        spacing: Kirigami.Units.largeSpacing

        Kirigami.Heading {
            text: QGlTr._("Which of these is <b>%1</b>?").arg(root.challengeData.meaningInSourceLanguage)
            wrapMode: Text.WordWrap
            leftPadding: Kirigami.Units.largeSpacing
            bottomPadding: Kirigami.Units.largeSpacing * 2

            Layout.fillWidth: true
        }

        Repeater {
            id: repeater

            model: root.challengeData.options

            delegate: Controls.RadioDelegate {
                property int modelIndex: model.index
                text: modelData
                Controls.ButtonGroup.group: answerGroup

                Layout.fillWidth: true
            }
        }
    }
}
