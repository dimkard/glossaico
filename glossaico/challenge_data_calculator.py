# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from typing import List, Dict
from slugify import slugify
from glossaico.glossaico_types import SkillData


class ChallengeDataCalculator:
    """Calculates the levels and the challenges properties of a skill"""

    def __init__(
        self,
        export_dir: str,
        data_validator,
        options_display_amt: int,
        cards_display_amt: int,
    ):
        self._skill_data = SkillData()
        self._validator = data_validator
        self._export_dir = export_dir
        self._options_display_amt = options_display_amt
        self._cards_display_amt = cards_display_amt
        self.loaded: bool = False

    def load(self, skill_data: SkillData):
        """Loads a specific skill for calculation of properties"""
        if (not skill_data.course_name) or (not skill_data.name):
            raise RuntimeError(
                "Missing skill information, progress can't be calculated"
            )

        self._skill_data = skill_data
        self.loaded = True

    def validateLoad(self):
        """Raise an error if skill data has not been loaded"""
        if not self.loaded:
            raise RuntimeError("No skill has been loaded for calculation")

    def validChallenges(self) -> List[Dict]:
        """Returns a list of the skill challenges that contain valid data"""
        self.validateLoad()
        challenge_data = self._challengeData()
        challenges = challenge_data["challenges"]
        return self._excludeInvalidCards(
            self._excludeInvalidOptions(
                self._excludeInvalidListening(challenges), self._skill_data.phrases
            ),
            self._skill_data.words,
        )

    def levels(self) -> int:
        """Returns the number of levels of the skill"""

        self.validateLoad()
        levels = 0
        challenge_data = self._challengeData()
        levels = challenge_data["levels"]

        if levels <= 0:
            raise RuntimeError(f"Levels set to an invalid number: {levels}")

        return levels

    def _excludeInvalidCards(self, challenges: List[Dict], words: list) -> List[Dict]:
        # Exclude card challenges if there are no sufficient answer candidates
        word_with_pictures = [w for w in words if w.pictures]
        if len(word_with_pictures) < self._cards_display_amt:
            challenges = [c for c in challenges if c["type"] != "cards"]

        # Exclude a cards challenge if no image file is found
        challenges = [
            c
            for c in challenges
            if (c["type"] != "cards")
            or (
                (c["type"] == "cards")
                and c["pictures"]
                and len(
                    [
                        p
                        for p in c["pictures"]
                        if not self._validator.imageExists(p, "tiny")
                    ]
                )
                == 0
            )
        ]

        return challenges

    def _excludeInvalidListening(self, challenges: List[Dict]) -> List[Dict]:
        # Exclude a listening challenge if no audio file is found
        challenges = [
            c
            for c in challenges
            if (c["type"] != "listeningExercise")
            or (
                (c["type"] == "listeningExercise")
                and (self._validator.voiceExists(c["audio"]))
            )
        ]

        return challenges

    def _excludeInvalidOptions(
        self, challenges: List[Dict], phrases: list
    ) -> List[Dict]:
        # Exclude option challenges if there are no sufficient answer candidates
        if len(phrases) < self._options_display_amt:
            challenges = [c for c in challenges if c["type"] != "options"]

        return challenges

    def _challengeData(self) -> dict:
        skill_challenges_file_path = (
            f"{self._export_dir}/{self._skill_data.course_name}/challenges/"
            f"{slugify(self._skill_data.name)}.json"
        )
        with open(skill_challenges_file_path, "r", encoding="utf-8") as challenge_file:
            challenge_file_data = json.load(challenge_file)

        return challenge_file_data
