# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QAbstractListModel, QModelIndex
from PySide2.QtCore import Qt, Slot
from glossaico.courses_info_loader import CoursesInfoLoader


class QCoursesModel(QAbstractListModel):
    """Class that exposes information about the available courses to QML"""

    NameRole = Qt.UserRole + 1
    SourceRole = Qt.UserRole + 2
    TargetRole = Qt.UserRole + 3
    DescriptionRole = Qt.UserRole + 4
    DeployedRole = Qt.UserRole + 5
    InProductionRole = Qt.UserRole + 6

    def __init__(self, courses_file_path, parent=None) -> None:
        super().__init__(parent)
        self._courses_json_path: str = courses_file_path
        self._courses: list = []

    @Slot(bool)
    def loadData(self, beta_courses: bool):
        """Loads data from the courses json file passed"""
        self.beginResetModel()
        self._courses = []
        if self._courses_json_path:
            loader = CoursesInfoLoader(self._courses_json_path)
            min_course_status = (
                CoursesInfoLoader.CourseStatus.BETA
                if beta_courses
                else CoursesInfoLoader.CourseStatus.STABLE
            )
            self._courses = loader.coursesInfo(min_course_status)
        self.endResetModel()

    def roleNames(self):
        """Returns the model's role names"""
        return {
            QCoursesModel.NameRole: b"name",
            QCoursesModel.SourceRole: b"source",
            QCoursesModel.TargetRole: b"target",
            QCoursesModel.DescriptionRole: b"description",
            QCoursesModel.DeployedRole: b"deployed",
            QCoursesModel.InProductionRole: b"inProduction",
        }

    def rowCount(self, parent=QModelIndex()):
        """Returns the number of rows under the given parent"""
        if parent.isValid():
            return 0

        return len(self._courses)

    def data(self, index, role):
        """Returns the data stored under the given role for the item referred to
        by the index
        """
        if not index.isValid():
            return None

        row = self._courses[index.row()]
        result = "Invalid Role"

        if role == Qt.DisplayRole:
            result = row["name"]
        elif role == QCoursesModel.NameRole:
            result = row["name"]
        elif role == QCoursesModel.SourceRole:
            result = row["source"]
        elif role == QCoursesModel.TargetRole:
            result = row["target"]
        elif role == QCoursesModel.DescriptionRole:
            result = row["description"]
        elif role == QCoursesModel.DeployedRole:
            result = row["deploy"]
        elif role == QCoursesModel.DescriptionRole:
            result = row["inProduction"]

        return result
