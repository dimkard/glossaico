# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass, asdict
from PySide2.QtCore import Signal, Slot, Property, QAbstractListModel, QModelIndex, Qt
from markdown import markdown
from glossaico.glossaico_types import SkillData
from glossaico.language_skill import LanguageSkill


@dataclass
class DisplaySkillData:
    """Exposes skill data to Qt"""

    name: str = ""
    introduction: str = ""
    sample_words: str = ""
    sample_phrases: str = ""
    sample_image: str = ""
    progress: float = 0
    completed_stale: bool = False


class QSkillsModel(QAbstractListModel):
    """Class that provides a model of the skills of a course"""

    DisplayObjectRole = Qt.UserRole + 1
    LanguageSkillRole = Qt.UserRole + 2

    def __init__(
        self, progress_calculator, challenge_data_calculator, parent=None
    ) -> None:
        super().__init__(parent)
        self._skills: list = []
        self._course_name = ""
        self._progress_calculator = progress_calculator
        self._challenge_data_calculator = challenge_data_calculator

    def roleNames(self):
        """Returns the model's role names"""
        return {
            QSkillsModel.DisplayObjectRole: b"skill_data",
            QSkillsModel.LanguageSkillRole: b"language_skill",
        }

    def rowCount(self, parent=QModelIndex()):
        """Returns the number of rows under the given parent"""
        if parent.isValid():
            return 0

        return len(self._skills)

    @staticmethod
    def sampleWords(row) -> str:
        """Creates a string of sample words"""
        if len(row.words) > 2:
            return (
                f"{row.words[0].in_target_language[0]}, "
                f"{row.words[1].in_target_language[0]}, "
                f"{row.words[2].in_target_language[0]}"
            )
        return ""

    @staticmethod
    def samplePhrases(row) -> str:
        """Creates a string of sample phrases"""
        if len(row.phrases) > 2:
            return (
                f"{row.phrases[0].in_target_language[0]}, "
                f"{row.phrases[1].in_target_language[0]}, "
                f"{row.phrases[2].in_target_language[0]}"
            )

        return ""

    def data(self, index, role):
        """Returns the data stored under the given role for the item referred to by the index"""
        if not index.isValid():
            return None

        row = self._skills[index.row()]

        skill_data = SkillData(
            skill_id=f"{row.id}",
            name=row.name,
            words=row.words,
            phrases=row.phrases,
            course_name=self._course_name,
        )

        language_skill = LanguageSkill(
            self._challenge_data_calculator, self._progress_calculator
        )
        language_skill.load(skill_data)

        skill_intro = markdown(row.introduction) if row.introduction else ""

        skill_image = row.image_set[0] if row.image_set else ""

        display_data = DisplaySkillData(
            name=language_skill.displayName,
            introduction=skill_intro,
            sample_image=skill_image,
            sample_words=self.__class__.sampleWords(row),
            sample_phrases=self.__class__.samplePhrases(row),
            progress=language_skill.progress,
            completed_stale=language_skill.needsReview,
        )

        if role == Qt.DisplayRole:
            return language_skill.displayName

        if role == QSkillsModel.DisplayObjectRole:
            return asdict(display_data)

        if role == QSkillsModel.LanguageSkillRole:
            return language_skill

        return "Invalid Role"

    def readSkills(self) -> list:
        """Returns the list that represents the skills of a course"""
        return self._skills

    def setSkills(self, val: list):
        """Sets the list that represents the skills of a course"""
        # pylint: disable=no-member
        self.beginResetModel()
        self._skills = val
        self.skillsChanged.emit()
        self.endResetModel()

    @Signal
    def skillsChanged(self):
        """Signal that is emitted when the skills list property is set or changed"""

    def readCourseName(self) -> str:
        """Returns the course name"""
        return self._course_name

    def setCourseName(self, val: str):
        """Sets the course name"""
        # pylint: disable=no-member
        self.beginResetModel()
        self._course_name = val
        self.courseNameChanged.emit()
        self.endResetModel()

    @Signal
    def courseNameChanged(self):
        """Emitted when the course name property is modified"""

    @Slot()
    def updateModel(self):
        """Slot that is called when the model is reset"""
        self.beginResetModel()
        self.endResetModel()

    skills = Property(list, readSkills, setSkills, notify=skillsChanged)
    courseName = Property(str, readCourseName, setCourseName, notify=courseNameChanged)
