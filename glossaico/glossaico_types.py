# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

"""List of types used in Glossaico"""

from dataclasses import dataclass, field


@dataclass
class SkillData:
    """Class that provides the data that a skill consists of"""

    skill_id: str = ""
    name: str = ""
    words: list = field(default_factory=list)
    phrases: list = field(default_factory=list)
    course_name: str = ""


@dataclass
class ChallengeQuestion:
    """Class that provides the basic data needed by challenge questions"""

    cid: str = ""
    priority: int = 0
    group: str = ""
    challengeType: str = ""


@dataclass
class ChallengeAnswer:
    """Class that provides the basic data needed by challenge answers"""

    cid: str = ""
    formInTargetLanguage: list = field(default_factory=list)


@dataclass
class ShortInputChallengeQuestion(ChallengeQuestion):
    """Class that provides the data needed by short input challenge questions"""

    meaningInSourceLanguage: str = ""
    pictures: list = field(default_factory=list)
    challengeType: str = "shortInput"


@dataclass
class OptionsChallengeQuestion(ChallengeQuestion):
    """Class that provides the data needed by options challenge questions"""

    meaningInSourceLanguage: str = ""
    options: list = field(default_factory=list)
    challengeType: str = "options"


@dataclass
class CardsChallengeQuestion(ChallengeQuestion):
    """Class that provides the data needed by cards challenge questions"""

    meaningInSourceLanguage: str = ""
    options: list = field(default_factory=list)
    challengeType: str = "cards"


@dataclass
class ListeningChallengeQuestion(ChallengeQuestion):
    """Class that provides the data needed by listening challenge questions"""

    meaning: str = ""
    answer: str = ""
    audio: str = ""
    challengeType: str = "listeningExercise"


@dataclass
class ChipsChallengeQuestion(ChallengeQuestion):
    """Class that provides the data needed by chips challenge questions"""

    meaningInSourceLanguage: str = ""
    chips: list = field(default_factory=list)
    formattedSolution: str = ""
    translatesToSourceLanguage: bool = True
    challengeType: str = "chips"
