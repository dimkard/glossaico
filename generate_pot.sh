#!/bin/sh

xgettext_exists() {
   if ! [ -x "$(command -v xgettext)" ]
   then
       echo "xgettext not found. Please install it and try again."
       return 1
   fi

   return 0
}

msgcat_exists() {
   if ! [ -x "$(command -v msgcat)" ]
   then
       echo "msgcat not found. Please install it and try again."
       return 1
   fi

   return 0
}


if xgettext_exists && msgcat_exists
then
    xgettext glossaico/qml/*.qml --from-code=UTF-8 --language="JavaScript" -o glossaico/po/glossaico_qml.pot
    xgettext glossaico/*.py --from-code=UTF-8 -o glossaico/po/glossaico_py.pot 
    msgcat glossaico/po/glossaico_qml.pot glossaico/po/glossaico_py.pot -o glossaico/po/glossaico.pot
    rm glossaico/po/glossaico_py.pot glossaico/po/glossaico_qml.pot
    echo "Translations template file has been generated"
fi

