# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime
import pytest
from sqlalchemy.orm import Session, clear_mappers
from sqlalchemy.sql import text
from glossaico.practice_session_alchemy_store import PracticeSessionAlchemyStore
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges
from glossaico.alchemy_database import AlchemyDatabase


@pytest.fixture(scope="module", name="dummy_db_session")
def fixture_dummy_db_session():
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    clear_mappers()
    engine = db_loader.load()
    db_session = Session(engine)

    return db_session


@pytest.fixture(scope="module", name="dummy_practice_session")
def fixture_dummy_practice_session(dummy_db_session):
    course = "dummy-course"
    cc_amt = 20
    ec_amt = 30
    skill_id = "dummy_skill_id"

    dummy_db_session.execute(
        text(
            "INSERT INTO practice_session (course, correct_challenges, expected_challenges,"
            "skill_id, practice_dt) VALUES(:cr, :ch, :e, :s, :dt)"
        ),
        {"cr": course, "ch": cc_amt, "e": ec_amt, "s": skill_id, "dt": datetime.now()},
    )

    dummy_db_session.commit()

    result = dummy_db_session.execute(
        text(
            "SELECT id from practice_session WHERE skill_id=:sk"
            " AND correct_challenges=:cc_amt AND expected_challenges=:ec_amt"
        ),
        {"sk": skill_id, "cc_amt": cc_amt, "ec_amt": ec_amt},
    )

    ps_id = result.first().id

    dummy_db_session.execute(
        text(
            "INSERT INTO passed_challenges (challenge_id, practice_id) "
            "VALUES (:cid, :pid)"
        ),
        [{"cid": "ch1", "pid": ps_id}, {"cid": "ch2", "pid": ps_id}],
    )

    return {
        "id": ps_id,
        "correct_challenges": cc_amt,
        "expected_challenges": ec_amt,
        "skill_id": skill_id,
        "passed": ["ch1", "ch2"],
        "course": "dummy-course",
    }


def test_get_inp_valid_record_id_out_correct_model(
    dummy_practice_session, dummy_db_session
):
    psas = PracticeSessionAlchemyStore(dummy_db_session)
    psd = psas.get(dummy_practice_session["id"])

    assert (
        (psd.skill_id == dummy_practice_session["skill_id"])
        and (psd.correct_challenges == dummy_practice_session["correct_challenges"])
        and (psd.expected_challenges == dummy_practice_session["expected_challenges"])
        and (len(psd.passed) == len(dummy_practice_session["passed"]))
    )


def test_getBySkillId_inp_valid_skill_id_out_correct_model(
    dummy_practice_session, dummy_db_session
):
    psas = PracticeSessionAlchemyStore(dummy_db_session)
    psd = psas.getBySkillId(
        dummy_practice_session["skill_id"], dummy_practice_session["course"]
    )[0]

    assert (
        (psd.skill_id == dummy_practice_session["skill_id"])
        and (psd.correct_challenges == dummy_practice_session["correct_challenges"])
        and (psd.expected_challenges == dummy_practice_session["expected_challenges"])
        and (len(psd.passed) == len(dummy_practice_session["passed"]))
    )


def test_add_inp_valid_model_out_record_inserted_to_db(dummy_db_session):
    store = PracticeSessionAlchemyStore(dummy_db_session)
    prc_data = PracticeSessionData(
        "dummy-course-2",
        datetime.now(),
        12,
        15,
        "skill_56",
        {PassedChallenges("ch3"), PassedChallenges("ch4")},
    )
    store.add(prc_data)
    dummy_db_session.commit()
    prc_data_retrieved = store.getBySkillId("skill_56", "dummy-course-2")[0]

    assert (prc_data_retrieved.skill_id == "skill_56") and (
        "ch4" in [c.challenge_id for c in prc_data.passed]
    )
