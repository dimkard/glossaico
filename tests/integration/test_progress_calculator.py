# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime, timedelta
import pytest
from sqlalchemy.orm import Session, clear_mappers
from glossaico.practice_session_alchemy_store import PracticeSessionAlchemyStore
from glossaico.practice_session_alchemy_store_keeper import (
    PracticeSessionAlchemyStoreKeeper,
)
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges
from glossaico.alchemy_database import AlchemyDatabase
from glossaico.progress_calculator import ProgressCalculator
from glossaico.glossaico_types import SkillData


@pytest.fixture(scope="module", name="db_engine")
def fixture_db_engine():
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    clear_mappers()
    engine = db_loader.load()

    return engine


@pytest.fixture(scope="module", name="non_completed_skill")
def fixture_non_completed_skill(db_engine):
    db_session = Session(db_engine)
    store = PracticeSessionAlchemyStore(db_session)

    skill_id = "id_nc_skill"
    course_name = "dummy_course"

    prc_data_1_dt = datetime(2022, 9, 12, 14, 0)
    prc_data_1 = PracticeSessionData(
        course_name,
        prc_data_1_dt,
        2,
        15,
        skill_id,
        {PassedChallenges("ch1"), PassedChallenges("ch2")},
    )
    store.add(prc_data_1)

    prc_data_2_dt = datetime(2022, 10, 2, 14, 30)
    prc_data_2 = PracticeSessionData(
        course_name,
        prc_data_2_dt,
        4,
        15,
        skill_id,
        {PassedChallenges("ch3"), PassedChallenges("ch4")},
    )
    store.add(prc_data_2)

    db_session.commit()

    return {
        "skill_data": SkillData(skill_id=skill_id, course_name=course_name),
        "last_date": prc_data_2_dt,
        "passed": {"ch1", "ch2", "ch3", "ch4"},
    }


@pytest.fixture(scope="module", name="non_completed_skill_levels")
def fixture_non_completed_skill_levels():
    return 3


@pytest.fixture(scope="module", name="completed_skill_review_needed")
def fixture_completed_skill_review_needed(db_engine):
    db_session = Session(db_engine)
    store = PracticeSessionAlchemyStore(db_session)

    skill_id = "id_cmp_skill_r"
    course_name = "dummy_course"

    prc_data_1_dt = datetime(2022, 9, 12, 14, 0)
    prc_data_1 = PracticeSessionData(
        course_name,
        prc_data_1_dt,
        10,
        10,
        skill_id,
        {
            PassedChallenges("r_cmp1"),
            PassedChallenges("r_cmp2"),
            PassedChallenges("r_cmp3"),
            PassedChallenges("r_cmp4"),
            PassedChallenges("r_cmp5"),
            PassedChallenges("r_cmp6"),
            PassedChallenges("r_cmp7"),
            PassedChallenges("r_cmp8"),
            PassedChallenges("r_cmp9"),
            PassedChallenges("r_cmp10"),
        },
    )
    store.add(prc_data_1)

    prc_data_2_dt = datetime(2022, 10, 2, 14, 30)
    prc_data_2 = PracticeSessionData(
        course_name,
        prc_data_2_dt,
        10,
        10,
        skill_id,
        {
            PassedChallenges("r_cmp1_2"),
            PassedChallenges("r_cmp2_2"),
            PassedChallenges("r_cmp3_2"),
            PassedChallenges("r_cmp4_2"),
            PassedChallenges("r_cmp5_2"),
            PassedChallenges("r_cmp6_2"),
            PassedChallenges("r_cmp7_2"),
            PassedChallenges("r_cmp8_2"),
            PassedChallenges("r_cmp9_2"),
            PassedChallenges("r_cmp10_2"),
        },
    )
    store.add(prc_data_2)

    prc_data_3_dt = datetime(2022, 10, 3, 14, 30)
    prc_data_3 = PracticeSessionData(
        course_name,
        prc_data_3_dt,
        10,
        10,
        skill_id,
        {
            PassedChallenges("r_cmp1_3"),
            PassedChallenges("r_cmp2_3"),
            PassedChallenges("r_cmp3_3"),
            PassedChallenges("r_cmp4_3"),
            PassedChallenges("r_cmp5_3"),
            PassedChallenges("r_cmp6_3"),
            PassedChallenges("r_cmp7_3"),
            PassedChallenges("r_cmp8_3"),
            PassedChallenges("r_cmp9_3"),
            PassedChallenges("r_cmp10_3"),
        },
    )
    store.add(prc_data_3)

    prc_data_4_dt = datetime.now() - timedelta(5)
    prc_data_4 = PracticeSessionData(
        course_name,
        prc_data_4_dt,
        10,
        10,
        skill_id,
        {
            PassedChallenges("r_cmp1_4"),
            PassedChallenges("r_cmp2_4"),
            PassedChallenges("r_cmp3_4"),
            PassedChallenges("r_cmp4_4"),
            PassedChallenges("r_cmp5_4"),
            PassedChallenges("r_cmp6_4"),
            PassedChallenges("r_cmp7_4"),
            PassedChallenges("r_cmp8_4"),
            PassedChallenges("r_cmp9_4"),
            PassedChallenges("r_cmp10_4"),
        },
    )
    store.add(prc_data_4)

    db_session.commit()

    return {
        "skill_data": SkillData(skill_id=skill_id, course_name=course_name),
        "last_date": prc_data_4_dt,
        "passed": {
            "r_cmp1",
            "r_cmp2",
            "r_cmp3",
            "r_cmp4",
            "r_cmp5",
            "r_cmp6",
            "r_cmp7",
            "r_cmp8",
            "r_cmp9",
            "r_cmp10",
            "r_cmp1_2",
            "r_cmp2_2",
            "r_cmp3_2",
            "r_cmp4_2",
            "r_cmp5_2",
            "r_cmp6_2",
            "r_cmp7_2",
            "r_cmp8_2",
            "r_cmp9_2",
            "r_cmp10_2",
            "r_cmp1_3",
            "r_cmp2_3",
            "r_cmp3_3",
            "r_cmp4_3",
            "r_cmp5_3",
            "r_cmp6_3",
            "r_cmp7_3",
            "r_cmp8_3",
            "r_cmp9_3",
            "r_cmp10_3",
            "r_cmp1_4",
            "r_cmp2_4",
            "r_cmp3_4",
            "r_cmp4_4",
            "r_cmp5_4",
            "r_cmp6_4",
            "r_cmp7_4",
            "r_cmp8_4",
            "r_cmp9_4",
            "r_cmp10_4",
        },
    }


@pytest.fixture(scope="module", name="completed_skill_no_review_needed")
def fixture_completed_skill_no_review_needed(db_engine):
    db_session = Session(db_engine)
    store = PracticeSessionAlchemyStore(db_session)

    skill_id = "id_cmp_skill_nr"
    course_name = "dummy_course"

    prc_data_1_dt = datetime(2022, 9, 12, 14, 0)
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_1_dt,
            10,
            15,
            skill_id,
            {
                PassedChallenges("cmp1"),
                PassedChallenges("cmp2"),
                PassedChallenges("cmp3"),
                PassedChallenges("cmp4"),
                PassedChallenges("cmp5"),
                PassedChallenges("cmp6"),
                PassedChallenges("cmp7"),
                PassedChallenges("cmp8"),
                PassedChallenges("cmp9"),
                PassedChallenges("cmp10"),
            },
        )
    )

    prc_data_2_dt = datetime(2022, 10, 2, 14, 30)
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_2_dt,
            12,
            15,
            skill_id,
            {
                PassedChallenges("cmp1_2"),
                PassedChallenges("cmp2_2"),
                PassedChallenges("cmp3_2"),
                PassedChallenges("cmp4_2"),
                PassedChallenges("cmp5_2"),
                PassedChallenges("cmp6_2"),
                PassedChallenges("cmp7_2"),
                PassedChallenges("cmp8_2"),
                PassedChallenges("cmp9_2"),
                PassedChallenges("cmp10_2"),
                PassedChallenges("cmp11_2"),
                PassedChallenges("cmp12_2"),
            },
        )
    )

    prc_data_3_dt = datetime(2022, 10, 3, 14, 30)
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_3_dt,
            12,
            15,
            skill_id,
            {
                PassedChallenges("cmp1_3"),
                PassedChallenges("cmp2_3"),
                PassedChallenges("cmp3_3"),
                PassedChallenges("cmp4_3"),
                PassedChallenges("cmp5_3"),
                PassedChallenges("cmp6_3"),
                PassedChallenges("cmp7_3"),
                PassedChallenges("cmp8_3"),
                PassedChallenges("cmp9_3"),
                PassedChallenges("cmp10_3"),
                PassedChallenges("cmp11_3"),
                PassedChallenges("cmp12_3"),
            },
        )
    )

    prc_data_4_dt = datetime(2022, 10, 4, 14, 30)
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_4_dt,
            12,
            15,
            skill_id,
            {
                PassedChallenges("cmp1_4"),
                PassedChallenges("cmp2_4"),
                PassedChallenges("cmp3_4"),
                PassedChallenges("cmp4_4"),
                PassedChallenges("cmp5_4"),
                PassedChallenges("cmp6_4"),
                PassedChallenges("cmp7_4"),
                PassedChallenges("cmp8_4"),
                PassedChallenges("cmp9_4"),
                PassedChallenges("cmp10_4"),
                PassedChallenges("cmp11_4"),
                PassedChallenges("cmp12_4"),
            },
        )
    )

    prc_data_5_dt = datetime(2022, 10, 5, 14, 30)
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_5_dt,
            12,
            15,
            skill_id,
            {
                PassedChallenges("cmp1_5"),
                PassedChallenges("cmp2_5"),
                PassedChallenges("cmp3_5"),
                PassedChallenges("cmp4_5"),
                PassedChallenges("cmp5_5"),
                PassedChallenges("cmp6_5"),
                PassedChallenges("cmp7_5"),
                PassedChallenges("cmp8_5"),
                PassedChallenges("cmp9_5"),
                PassedChallenges("cmp10_5"),
                PassedChallenges("cmp11_5"),
                PassedChallenges("cmp12_5"),
            },
        )
    )

    prc_data_6_dt = datetime.now()
    store.add(
        PracticeSessionData(
            course_name,
            prc_data_6_dt,
            5,
            10,
            skill_id,
            {
                PassedChallenges("cmp1_6"),
                PassedChallenges("cmp2_6"),
                PassedChallenges("cmp3_6"),
                PassedChallenges("cmp4_6"),
                PassedChallenges("cmp5_6"),
            },
        )
    )

    db_session.commit()

    return {
        "skill_data": SkillData(skill_id=skill_id, course_name=course_name),
        "last_date": prc_data_6_dt,
        "passed": {
            "cmp1",
            "cmp2",
            "cmp3",
            "cmp4",
            "cmp5",
            "cmp6",
            "cmp7",
            "cmp8",
            "cmp9",
            "cmp10",
            "cmp1_2",
            "cmp2_2",
            "cmp3_2",
            "cmp4_2",
            "cmp5_2",
            "cmp6_2",
            "cmp7_2",
            "cmp8_2",
            "cmp9_2",
            "cmp10_2",
            "cmp11_2",
            "cmp12_2",
            "cmp1_3",
            "cmp2_3",
            "cmp3_3",
            "cmp4_3",
            "cmp5_3",
            "cmp6_3",
            "cmp7_3",
            "cmp8_3",
            "cmp9_3",
            "cmp10_3",
            "cmp11_3",
            "cmp12_3",
            "cmp1_4",
            "cmp2_4",
            "cmp3_4",
            "cmp4_4",
            "cmp5_4",
            "cmp6_4",
            "cmp7_4",
            "cmp8_4",
            "cmp9_4",
            "cmp10_4",
            "cmp11_4",
            "cmp12_4",
            "cmp12_5",
            "cmp1_5",
            "cmp2_5",
            "cmp3_5",
            "cmp4_5",
            "cmp5_5",
            "cmp6_5",
            "cmp7_5",
            "cmp8_5",
            "cmp9_5",
            "cmp10_5",
            "cmp11_5",
            "cmp1_6",
            "cmp2_6",
            "cmp3_6",
            "cmp4_6",
            "cmp5_6",
        },
    }


@pytest.fixture(scope="module", name="completed_skill_levels")
def fixture_completed_skill_levels():
    return 4


@pytest.fixture(scope="function", name="calculator")
def fixture_calculator(db_engine):
    keeper = PracticeSessionAlchemyStoreKeeper(db_engine)
    return ProgressCalculator(keeper)


def test_skillProgress_inp_non_loaded_calculator_out_error(calculator):
    with pytest.raises(RuntimeError):
        calculator.skillProgress()


def test_needsReview_inp_non_loaded_calculator_out_error(calculator):
    with pytest.raises(RuntimeError):
        calculator.needsReview()


def test_challengesPassed_inp_non_loaded_calculator_out_error(calculator):
    with pytest.raises(RuntimeError):
        calculator.challengesPassed()


def test_skillProgress_inp_non_completed_skill_out_valid_number(
    calculator, non_completed_skill, non_completed_skill_levels
):
    calculator.load(non_completed_skill["skill_data"], non_completed_skill_levels)
    progress = calculator.skillProgress()

    assert 0 < progress < 1


def test_skillProgress_inp_completed_skill_no_review_needed_out_valid_number(
    calculator, completed_skill_no_review_needed, completed_skill_levels
):
    calculator.load(
        completed_skill_no_review_needed["skill_data"], completed_skill_levels
    )
    progress = calculator.skillProgress()

    assert progress >= 1


def test_skillProgress_inp_completed_skill_review_needed_out_valid_number(
    calculator, completed_skill_review_needed, completed_skill_levels
):
    calculator.load(completed_skill_review_needed["skill_data"], completed_skill_levels)
    progress = calculator.skillProgress()

    assert progress >= 1


def test_lastPracticeDate_inp_valid_skill_out_valid_date(
    calculator, non_completed_skill, non_completed_skill_levels
):

    calculator.load(non_completed_skill["skill_data"], non_completed_skill_levels)
    last_practice_dt = calculator.lastPracticeDate()

    assert last_practice_dt == non_completed_skill["last_date"]


def test_needsReview_inp_non_completed_skill_out_False(
    calculator, non_completed_skill, non_completed_skill_levels
):
    calculator.load(non_completed_skill["skill_data"], non_completed_skill_levels)
    needs_review = calculator.needsReview()

    assert not needs_review


def test_needsReview_inp_completed_no_review_needed_skill_out_False(
    calculator, completed_skill_no_review_needed, non_completed_skill_levels
):
    calculator.load(
        completed_skill_no_review_needed["skill_data"], non_completed_skill_levels
    )
    needs_review = calculator.needsReview()

    assert not needs_review


def test_needsReview_inp_completed_review_needed_skill_out_True(
    calculator, completed_skill_review_needed, non_completed_skill_levels
):
    calculator.load(
        completed_skill_review_needed["skill_data"], non_completed_skill_levels
    )
    needs_review = calculator.needsReview()

    assert needs_review


def test_challengesPassed_inp_non_completed_skill_out_valid_set_of_challenge_ids(
    calculator, non_completed_skill, non_completed_skill_levels
):
    calculator.load(non_completed_skill["skill_data"], non_completed_skill_levels)
    passed_challenge_ids = calculator.challengesPassed()

    assert passed_challenge_ids == non_completed_skill["passed"]


def test_challengesPassed_inp_completed_skill_nr_out_valid_set_of_challenge_ids(
    calculator, completed_skill_no_review_needed, completed_skill_levels
):
    calculator.load(
        completed_skill_no_review_needed["skill_data"], completed_skill_levels
    )
    passed_challenge_ids = calculator.challengesPassed()

    assert passed_challenge_ids == completed_skill_no_review_needed["passed"]


def test_challengesPassed_inp_completed_skill_r_out_valid_set_of_challenge_ids(
    calculator, completed_skill_review_needed, completed_skill_levels
):
    calculator.load(completed_skill_review_needed["skill_data"], completed_skill_levels)
    passed_challenge_ids = calculator.challengesPassed()

    assert passed_challenge_ids == completed_skill_review_needed["passed"]
