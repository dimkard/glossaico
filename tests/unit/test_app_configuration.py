# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.app_configuration import ApplicationConfiguration


@pytest.fixture(scope="function", name="dummy_config")
def fixture_dummy_config():
    return ApplicationConfiguration("test-sum-1", True, "v1", False)


def test_read_config_version_inp_valid_str_out_correct_value(dummy_config):
    assert dummy_config.configVersion == "v1"


def test_read_installed_data_checksum_inp_valid_str_out_correct_value(dummy_config):
    assert dummy_config.installedDataCheckSum == "test-sum-1"


def test_read_silent_mode_inp_valid_str_out_correct_value(dummy_config):
    assert dummy_config.silentMode


def test_read_show_beta_courses_inp_valid_str_out_correct_value(dummy_config):
    assert not dummy_config.showBetaCourses


def test_set_config_version_inp_valid_str_out_correct_value(dummy_config):
    dummy_config.configVersion = "v3"

    assert dummy_config.configVersion == "v3"


def test_set_installed_data_checksum_inp_valid_str_out_correct_value(dummy_config):
    dummy_config.installedDataCheckSum = "test-sum-2"

    assert dummy_config.installedDataCheckSum == "test-sum-2"


def test_set_silent_mode_inp_valid_str_out_correct_value(dummy_config):
    dummy_config.silentMode = True

    assert dummy_config.silentMode


def test_set_show_beta_courses_inp_valid_str_out_correct_value(dummy_config):
    dummy_config.showBetaCourses = True

    assert dummy_config.showBetaCourses
