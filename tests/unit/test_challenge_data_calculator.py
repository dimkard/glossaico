# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import json
import pytest
from glossaico.glossaico_types import SkillData
from glossaico.challenge_data_calculator import ChallengeDataCalculator


@pytest.fixture(scope="module", name="export_dir")
def fixture_export_dir(tmp_path_factory):
    export_dir = tmp_path_factory.mktemp("export")

    return export_dir


@pytest.fixture(scope="module", name="ready_skill")
def fixture_ready_skill(export_dir):
    challenges_dict = {
        "id": "569673478a3d",
        "levels": 4,
        "challenges": [
            {
                "type": "options",
                "formInTargetLanguage": "Buen provecho",
                "meaningInSourceLanguage": "Enjoy your meal",
                "id": "084d1bcf7750",
                "priority": 0,
                "group": "241cb10c25a8",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "Enjoy", "definition": "disfruta"},
                    {"word": "your", "definition": "su\ntu"},
                    {"word": "meal", "definition": "comida"},
                ],
                "chips": ["Buen", "provecho", "eres", "mucho"],
                "solutions": [["Buen", "provecho"]],
                "formattedSolution": "Buen provecho",
                "id": "e411efcec0af",
                "priority": 2,
                "group": "241cb10c25a8",
            },
        ],
    }

    course_name = "dummy-course"
    skill_name = "Verbs"

    course_challenge_dir = f"{export_dir}/{course_name}/challenges"
    Path(course_challenge_dir).mkdir(parents=True)
    challenge_path = Path(f"{course_challenge_dir}/verbs.json")
    with open(challenge_path, "w", encoding="utf-8") as challenge_file:
        json.dump(challenges_dict, challenge_file)

    return SkillData(course_name=course_name, name=skill_name)


def test_validChallenges_inp_no_skill_data_loaded_out_error(export_dir):
    calculator = ChallengeDataCalculator(export_dir, None, 3, 4)
    with pytest.raises(RuntimeError):
        calculator.validChallenges()


def test_levels_inp_no_skill_data_loaded_out_error(export_dir):
    calculator = ChallengeDataCalculator(export_dir, None, 3, 4)
    with pytest.raises(RuntimeError):
        calculator.levels()


def test_validChallenges_inp_valid_json_file_no_validation_out_correct_amt_of_challenges(
    ready_skill, export_dir
):
    calculator = ChallengeDataCalculator(export_dir, None, 3, 4)
    calculator._excludeinvalidcards = lambda x, y: x
    calculator._excludeInvalidListening = lambda x: x
    calculator._excludeInvalidOptions = lambda x, y: x
    calculator.load(ready_skill)

    assert len(calculator.validChallenges()) == 2


def test_levels_inp_valid_json_file_no_validation_out_correct_levels(
    ready_skill, export_dir
):
    calculator = ChallengeDataCalculator(export_dir, None, 3, 4)
    calculator._excludeinvalidcards = lambda x, y: x
    calculator._excludeInvalidListening = lambda x: x
    calculator._excludeInvalidOptions = lambda x, y: x
    calculator.load(ready_skill)

    assert calculator.levels() == 4
