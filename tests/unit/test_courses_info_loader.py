# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.courses_info_loader import CoursesInfoLoader


@pytest.fixture(scope="module", name="courses_loader")
def fixture_courses_loader(courses_json_path):
    return CoursesInfoLoader(courses_json_path)


def test_coursesInfo_inp_stable_status_out_correct_amt(courses_loader):
    courses = courses_loader.coursesInfo(CoursesInfoLoader.CourseStatus.STABLE)

    assert len(courses) == 1


def test_coursesInfo_inp_at_least_beta_status_out_correct_amt(courses_loader):
    courses = courses_loader.coursesInfo(CoursesInfoLoader.CourseStatus.BETA)

    assert len(courses) == 2
