# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


import json
import pytest


@pytest.fixture(scope="module", name="courses_json_path")
def fixture_courses_json_path(tmp_path_factory):
    data_dir = tmp_path_factory.mktemp("data_path")
    courses_json_obj = [
        {
            "url": "https://github.com/LibreLingo/LibreLingo-ES-from-EN/archive/refs/heads/main.zip",
            "repositoryURL": "https://github.com/LibreLingo/LibreLingo-ES-from-EN",
            "paths": {
                "yamlFolder": "LibreLingo-ES-from-EN-main",
                "jsonFolder": "spanish-from-english",
            },
            "deploy": True,
            "devtoolsEnabled": True,
            "inProduction": True,
            "name": "spanish-from-english",
            "source": "English",
            "target": "Spanish",
            "description": "Spanish for English speakers",
        },
        {
            "url": "https://github.com/szabgab/LibreLingo-Judeo-Spanish-from-English/archive/refs/heads/main.zip",
            "repositoryURL": "https://github.com/szabgab/LibreLingo-Judeo-Spanish-from-English",
            "paths": {
                "yamlFolder": "LibreLingo-Judeo-Spanish-from-English-main",
                "jsonFolder": "ladino-from-english",
            },
            "deploy": False,
            "devtoolsEnabled": False,
            "inProduction": False,
            "name": "ladino-from-english",
            "source": "English",
            "target": "Ladino",
            "description": "Ladino for English speakers",
        },
        {
            "url": "https://github.com/szabgab/LibreLingo-Hungarian-from-Spanish/archive/refs/heads/main.zip",
            "repositoryURL": "https://github.com/szabgab/LibreLingo-Hungarian-from-Spanish",
            "paths": {
                "yamlFolder": "LibreLingo-Hungarian-from-Spanish-main",
                "jsonFolder": "hungarian-from-spanish",
            },
            "deploy": False,
            "devtoolsEnabled": True,
            "inProduction": False,
            "name": "hungarian-from-spanish",
            "source": "Spanish",
            "target": "Hungarian",
            "description": "Hungarian for Spanish speakers",
        },
        {
            "url": "https://codeberg.org/kate/LibreLingo_FR_from_EN/archive/master.zip",
            "repositoryURL": "https://codeberg.org/kate/LibreLingo_FR_from_EN",
            "paths": {
                "yamlFolder": "LibreLingo-FR-from-EN",
                "jsonFolder": "french-from-english",
            },
            "deploy": True,
            "devtoolsEnabled": False,
            "inProduction": False,
            "name": "french-from-english",
            "source": "English",
            "target": "French",
            "description": "French for English speakers",
        },
    ]
    courses_json_path = f"{data_dir}/courses.json"
    with open(courses_json_path, "w", encoding="utf-8") as json_file:
        json.dump(courses_json_obj, json_file)

    return courses_json_path
