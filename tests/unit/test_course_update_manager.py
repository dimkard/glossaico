# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


from unittest.mock import Mock
from pathlib import Path
from shutil import make_archive, rmtree
import pytest
from glossaico.course_update_manager import CourseUpdateManager


@pytest.fixture(scope="function", name="dummy_upd_manager")
def fixture_dummy_upd_manager(tmp_path_factory):
    app_data_dir = tmp_path_factory.mktemp("data_dir")
    cache_dir = tmp_path_factory.mktemp("cache_dir")

    upd_manager = CourseUpdateManager(
        app_data_dir.resolve(), cache_dir.resolve(), "dummy-local-checksum"
    )

    return upd_manager


def test_updateCourseContent_inp_invalid_remote_url_out_error_msg(dummy_upd_manager):
    dummy_upd_manager.remoteDataArchiveUrl = Mock(return_value="http://path/to/")
    dummy_upd_manager.remoteDataArchiveChecksum = Mock(
        return_value="dummy-remote-checksum"
    )
    with pytest.raises(RuntimeError):
        dummy_upd_manager.updateCourseContent()


def test_updateCourseContent_inp_valid_archive_out_new_data_exist_in_target_dir(
    dummy_upd_manager,
):
    cache_dir = dummy_upd_manager._cache_dir
    data_dir = dummy_upd_manager._app_data_dir

    # New updated data
    new_data_base_dir = f"{cache_dir}/new-data/"
    Path(new_data_base_dir).mkdir(parents=True)
    Path(f"{new_data_base_dir}/glossaico-data/courses/new-course/courses/").mkdir(
        parents=True
    )
    Path(f"{new_data_base_dir}/glossaico-data/voice/").mkdir(parents=True)
    Path(f"{new_data_base_dir}/glossaico-data/images/").mkdir(parents=True)

    with open(
        f"{new_data_base_dir}/glossaico-data/courses.json", "w", encoding="utf-8"
    ) as f:
        f.write("{}")

    new_data_archive = f"{cache_dir}/new_data_archive"
    make_archive(new_data_archive, "zip", new_data_base_dir)
    rmtree(new_data_base_dir)
    Path(f"{data_dir}/courses/old-course").mkdir(parents=True)

    dummy_upd_manager.remoteDataArchiveUrl = Mock(return_value="http://localhost/")
    dummy_upd_manager.remoteDataArchiveChecksum = Mock(
        return_value="dummy-remote-checksum"
    )
    dummy_upd_manager._fetchRemoteArchive = Mock(return_value=f"{new_data_archive}.zip")

    dummy_upd_manager.updateCourseContent()

    assert Path(f"{data_dir}/courses/new-course").is_dir()
