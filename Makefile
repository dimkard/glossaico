format:
	python3 -m black glossaico/ tests/

typehint:
	python3 -m mypy glossaico/ tests/

test:
	python3 -m pytest tests/

lint:
	python3 -m pylint --rcfile=glossaico/pylintrc glossaico/ 
	python3 -m pylint --rcfile=tests/pylintrc tests/unit/
	python3 -m pylint --rcfile=tests/pylintrc tests/integration/
	python3 -m pylint --rcfile=tests/pylintrc setup.py

reuse:
	python3 -m reuse lint

all:
	make -i format
	make -i lint
	make -i typehint
	make -i test
	make -i reuse
	./generate_pot.sh
.PHONY: format typehint test lint checklist

